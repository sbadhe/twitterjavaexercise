package org.interview.util;

/**
 * 
 * Constant values to be use through apllication.
 * 
 * @author Sagar Badhe
 * 
 *
 */
public class Constants {

	/**
	 * Query parameter has to pass in twitter query
	 */
	public static String REQUESTPARAMETERTRACK = "track";
}
