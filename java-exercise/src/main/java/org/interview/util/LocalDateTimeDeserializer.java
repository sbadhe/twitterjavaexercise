package org.interview.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.interview.twitter.controller.TwitterController;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * When we mapped Json response to object we have to deserialized Json Date
 * string to LocalDateTime . This is customize deserialization class for
 * LocalDateTime.
 * 
 * @author Sagar Badhe
 *
 */
public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
	
	private static final Logger LOGGER = Logger.getLogger(LocalDateTimeDeserializer.class);
	
	@Override
	public LocalDateTime deserialize(JsonParser arg0, DeserializationContext arg1)
			throws IOException, JsonProcessingException {

		SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH);

		formatter.setLenient(true);

		try {
			return LocalDateTime.ofInstant(Instant.ofEpochMilli(formatter.parse(arg0.getText()).getTime()),
					ZoneId.systemDefault());
		} catch (ParseException e) {
			LOGGER.error("Date Parsing Error While Converting Json Date string to object ");
		}
		return null;
	}
}