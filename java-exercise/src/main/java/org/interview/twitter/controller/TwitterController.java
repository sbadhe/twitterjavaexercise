package org.interview.twitter.controller;

import org.apache.log4j.Logger;
import org.interview.twitter.exception.TwitterAuthenticationException;
import org.interview.twitter.exception.TwitterServiceException;
import org.interview.twitter.oauth.TwitterAuthenticator;
import org.interview.twitter.service.TwitterStreamingService;
import org.interview.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * Main controller for handle all the client request to process with Twitter.
 * 
 * @author Sagar Badhe
 *
 */
@RestController
@RequestMapping("/tweet")
public class TwitterController {

	private static final Logger LOGGER = Logger.getLogger(TwitterController.class);

	/**
	 * Key for twiter account
	 */
	@Value("${consumerKey}")
	private String consumerKey;

	/**
	 * Password
	 */
	@Value("${consumerPassword}")
	private String consumerPassword;

	/**
	 * Query for twitter streaming messages
	 */
	@Value("${searchUrl}")
	private String searchUrl;

	/**
	 * Streaming service
	 */
	@Autowired
	TwitterStreamingService twitterService;

	/**
	 * This method gets the request from client to stream through all the tweets
	 * 
	 * @param searchString
	 */
	@RequestMapping(value = "stream/{searchString}", method = RequestMethod.GET)
	public void streamThroughTweets(@PathVariable("searchString") String searchString)
			throws TwitterAuthenticationException, TwitterServiceException {

		final String searchQuery = searchUrl + "?" + Constants.REQUESTPARAMETERTRACK + "=" + searchString;

		LOGGER.debug("consumerKey : " + consumerKey);
		LOGGER.debug("consumerPassword : " + consumerPassword);

		LOGGER.debug("Query : " + searchString);
		final TwitterAuthenticator twitterAuthenticator = new TwitterAuthenticator(System.out, consumerKey,
				consumerPassword);

		twitterService.listOfTweets(twitterAuthenticator.getAuthorizedHttpRequestFactory(), searchQuery).forEach(m -> {
			LOGGER.info(" User Name : " + m.getUser().getName() + " : User Created Date : " +m.getUser().getCreated_at() + " : Message By User : "+ m.getText() );
		});

	}

}
