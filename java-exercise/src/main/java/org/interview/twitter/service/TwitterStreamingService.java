package org.interview.twitter.service;

import java.io.IOException;
import java.util.List;

import org.interview.twitter.exception.TwitterServiceException;
import org.interview.twitter.pojo.Message;

import com.google.api.client.http.HttpRequestFactory;

/**
 * 
 * Service to perform request and response of twitters
 * 
 * @author sagar badhe
 *
 */
public interface TwitterStreamingService {
	/**
	 * get list of all the tweets by query passed.
	 * 
	 * @param httpRequestFactory
	 *            Authenticated Request factory
	 * @param searchQuery
	 *            to search from Twitter
	 * @return List of all the tweets
	 * @throws IOException
	 */
	public List<Message> listOfTweets(final HttpRequestFactory httpRequestFactory, final String searchQuery)
			throws TwitterServiceException;
}
