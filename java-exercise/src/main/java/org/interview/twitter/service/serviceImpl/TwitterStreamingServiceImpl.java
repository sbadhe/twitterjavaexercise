package org.interview.twitter.service.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.interview.twitter.exception.TwitterServiceException;
import org.interview.twitter.pojo.Message;
import org.interview.twitter.service.TwitterStreamingService;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;

@Service
public class TwitterStreamingServiceImpl implements TwitterStreamingService {

	@Override
	public List<Message> listOfTweets(HttpRequestFactory httpRequestFactory, String searchQuery)
			throws TwitterServiceException {

		HttpRequest request;
		final List<Message> listOfMessageRecieved = new ArrayList<Message>();
		final long startTime = System.currentTimeMillis();

		try {
			request = httpRequestFactory.buildGetRequest(new GenericUrl(searchQuery));

			final HttpResponse twitterOutputResponse = request.execute();
			// CONVERT RESPONSE TO STRING
			BufferedReader reader = new BufferedReader(new InputStreamReader(twitterOutputResponse.getContent()));
			
			while (listOfMessageRecieved.size() < 100 && System.currentTimeMillis() - startTime < 30000) {

				String jsonString = reader.readLine();

				ObjectMapper mapper = new ObjectMapper();
				mapper.registerModule(new JavaTimeModule());

				// JSON from String to Object
				final Message message = mapper.readValue(jsonString, Message.class);
				listOfMessageRecieved.add(message);
			}			

		} catch (IOException ioException) {
		 throw new TwitterServiceException("0", "Error in Twitter Service cose ");
		}
		return sortMessages(listOfMessageRecieved);
	}

	/**
	 * Sort Message first base on the name of user(that will group message on
	 * user), then user order user by creation date and then sort by message
	 * created
	 * 
	 * @param listOfMessageRecieved
	 */
	private List<Message> sortMessages(final List<Message> listOfMessageRecieved) {

		Comparator<Message> autherNameSort = (message1, message2) -> message1.getUser().getName()
				.compareTo(message2.getUser().getName());
		Comparator<Message> autherCreatedDateSort = (message1, message2) -> message1.getUser().getCreated_at()
				.compareTo(message2.getUser().getCreated_at());
		Comparator<Message> messageCreatedDateSort = (message1, message2) -> message1.getCreated_at()
				.compareTo(message2.getCreated_at());
		listOfMessageRecieved
				.sort(autherNameSort.thenComparing(autherCreatedDateSort).thenComparing(messageCreatedDateSort));

		return listOfMessageRecieved;
	}

}
