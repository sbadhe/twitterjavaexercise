package org.interview.twitter.exception;

@SuppressWarnings("serial")
public class TwitterServiceException extends RuntimeException{

	String errCode;
	
	public TwitterServiceException() {
		super();
	}
	
	public TwitterServiceException(final String errCode) {
		this.errCode = errCode;
	}
	
	public TwitterServiceException(final String errCode, final String message) {
		super(message);
		this.errCode = errCode;
	}
	
	public TwitterServiceException(final String message,final  Throwable throwable) {
		super(message,throwable);
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(final String errCode) {
		this.errCode = errCode;
	}
}