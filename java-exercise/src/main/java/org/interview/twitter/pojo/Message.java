package org.interview.twitter.pojo;

import java.time.LocalDateTime;

import org.interview.util.LocalDateTimeDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Pojo class for Message generated
 * 
 * @author Sagar Badhe Created on 26-03
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Message {

	/**
	 * Message ID
	 */
	private long id;
	
	/**
	 * Message created date
	 */
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime created_at;

	/**
	 * Message Text
	 */
	private String text;
	

	/**
	 * User object for this particular object
	 */
	private Auther user;


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the created_at
	 */
	public LocalDateTime getCreated_at() {
		return created_at;
	}

	/**
	 * @param created_at
	 *            the created_at to set
	 */
	public void setCreated_at(LocalDateTime created_at) {
		this.created_at = created_at;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the user
	 */
	public Auther getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(Auther user) {
		this.user = user;
	}

	
}
