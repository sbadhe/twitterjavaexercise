package org.interview.twitter.pojo;

import java.time.LocalDateTime;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.interview.util.LocalDateTimeDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * User of Twitter who post message
 * 
 * @author Sagar Badhe
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Auther {

	/**
	 * Id of user
	 */
	private long id;

	/**
	 * User created Date
	 */
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime created_at;
	/**
	 * Name of the user
	 */
	private String name;
	/**
	 * Screen
	 */
	private String screen_name;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the created_at
	 */
	public LocalDateTime getCreated_at() {
		return created_at;
	}

	/**
	 * @param created_at
	 *            the created_at to set
	 */
	public void setCreated_at(LocalDateTime created_at) {
		this.created_at = created_at;
	}

	/**
	 * @return the screen_name
	 */
	public String getScreen_name() {
		return screen_name;
	}

	/**
	 * @param screen_name
	 *            the screen_name to set
	 */
	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}

}
